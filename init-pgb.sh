echo Initializing "$1"...
mkdir ./"$1"
cp ./project_template/main.cpp ./"$1"/main.cpp

echo Copied Project-files!
echo Creating directory-structure...

mkdir -p ./"$1"/"$1"_app/include
mkdir -p ./"$1"/"$1"_app/src
mkdir -p ./"$1"/"$1"_app/tests

mkdir -p ./"$1"/"$1"_lib/include
mkdir -p ./"$1"/"$1"_lib/src
mkdir -p ./"$1"/"$1"_lib/tests
touch ./"$1"/"$1"_lib/"$1".hpp

mkdir -p ./"$1"/gui_lib/include
mkdir -p ./"$1"/gui_lib/src
mkdir -p ./"$1"/gui_lib/tests

echo [DONE!]

echo Updating CMake-Project...
sed "s/<target>/$1/g" ./project_template/CMakeLists.txt > ./"$1"/CMakeLists.txt
echo New PGB-project "$1" successfully initialized. Exiting now.
exit
