//
// Created by david on 14.10.19.
//

#include "EnrollDialog.h"

void EnrollDialog::initialize() {
    layout = new QVBoxLayout(this);

    labelFirstName = new QLabel("First name");
    editFirstName = new QLineEdit(this->firstName, this);
    labelLastName = new QLabel("Last name");
    editLastName = new QLineEdit (this->lastName, this);
    labelId = new QLabel("ID Number");
    editId = new QLineEdit(this->id, this);
    buttonBox = new QDialogButtonBox(QDialogButtonBox::Save | QDialogButtonBox::Cancel, this);

    layout->addWidget(labelFirstName);
    layout->addWidget(editFirstName);
    layout->addWidget(labelLastName);
    layout->addWidget(editLastName);
    layout->addWidget(labelId);
    layout->addWidget(editId);
    layout->addWidget(buttonBox);

    this->setLayout(layout);

    connect(buttonBox, &QDialogButtonBox::accepted, this, &EnrollDialog::saveButtonClicked);
}

void EnrollDialog::show() {
    initialize();
    base->show();
}

void EnrollDialog::saveButtonClicked() {
    printf("Hallo Welt!");
}

void EnrollDialog::show(QString _firstName, QString _lastName, QString _id) {
    this->firstName = _firstName;
    this->lastName = _lastName;
    this->id = _id;
    show();
}