//
// Created by david on 14.10.19.
//

#ifndef PRAKTIKUM1_ENROLLDIALOG_H
#define PRAKTIKUM1_ENROLLDIALOG_H


#include <QtWidgets/QDialog>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QDialogButtonBox>


class EnrollDialog : public QDialog {
private:
    QString firstName;
    QString lastName;
    QString id;

    QDialog *base = this;
    QVBoxLayout *layout;
    QLineEdit *editFirstName;
    QLineEdit *editLastName;
    QLineEdit *editId;
    QDialogButtonBox *buttonBox;
    QLabel *labelFirstName;
    QLabel *labelLastName;
    QLabel *labelId;

    void initialize();

public:
    void show();
    void show(QString _firstName, QString _lastName, QString _id);
private slots:
    static void saveButtonClicked();
    void cancelButtonClicked();
};


#endif //PRAKTIKUM1_ENROLLDIALOG_H
