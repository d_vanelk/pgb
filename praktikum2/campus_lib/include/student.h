//
// Created by david on 15.10.19.
//
#include <string>

#ifndef PRAKTIKUM2_STUDENT_H
#define PRAKTIKUM2_STUDENT_H

#endif //PRAKTIKUM2_STUDENT_H

struct Student {
    std::string firstName;
    std::string lastName;
    unsigned int id;
};

std::ostream& operator << (std::ostream& os, const Student& student) {
    os << student.firstName << " " << student.lastName << " (" << student.id << ")";
    return os;
}