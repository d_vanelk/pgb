#include <iostream>
#include "campus_lib/campus_lib.h"

int main() {
    Student s;

    s.firstName = "John";
    s.lastName = "Appleseed";
    s.id = 4611;

    std::cout << s << std::endl;
    std::cout << "Hello, World!" << std::endl;
    return 0;
}