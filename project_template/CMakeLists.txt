cmake_minimum_required(VERSION 3.15)
project(<target>)

set(CMAKE_CXX_STANDARD 14)

add_executable(<target> main.cpp)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

if (CMAKE_VERSION VERSION_LESS "3.7.0")
    set(CMAKE_INCLUDE_CURRENT_DIR ON)
endif ()

find_package(Qt5 COMPONENTS Widgets REQUIRED)

target_link_libraries(<target> Qt5::Widgets)
